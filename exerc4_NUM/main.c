#include "global.h"
#include "funcs.h"
#include "scan.h"
#include "scan.c"

int Error = FALSE;

FILE * source;

int buffer_size;

int isAlpha(char c)
{
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int isDigit(char c)
{
        return (c >= '0' && c <= '9');
}

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        Error = TRUE;
    }

    if (!Error) {
        source = fopen(argv[1], "r");

        if (source == NULL) {
            printf("Erro ao alocar memoria para o arquivo texto\n");
            Error = FALSE;
        }
    }

    if (!Error) {
        bufferStruct * buffer = allocate_buffer();
        lexemaStruct * lexema = allocate_lexema();

        if (buffer == NULL || lexema == NULL) {
            printf("Memoria insuficiente para alocacao do buffer");
            exit(1);
        }
        else {
            char caracter;

            while ((caracter = get_next_char(buffer)) != EOF) {
                if (isAlpha(caracter)) {
                    lexema->posLexema++;
                    lexema->lexema[lexema->posLexema] = caracter;

                    while (isAlpha(caracter = get_next_char(buffer))) {
                        lexema->posLexema++;
                        lexema->lexema[lexema->posLexema] = caracter;
                    }

                    if (lexema->posLexema < 63) {
                        lexema->lexema[lexema->posLexema + 1] = '\0';
                    }
                    if (lexema->posLexema > -1) {
                        if (!strcmp(lexema->lexema, "if")) {
                            lexema->lexemaToken = IF;
                        } else if (!strcmp(lexema->lexema, "else")) {
                            lexema->lexemaToken = ELSE;
                        } else if (!strcmp(lexema->lexema, "while")) {
                            lexema->lexemaToken = WHILE;
                        } else if (!strcmp(lexema->lexema, "do")) {
                            lexema->lexemaToken = DO;
                        } else if (!strcmp(lexema->lexema, "for")) {
                            lexema->lexemaToken = FOR;
                        } else if (!strcmp(lexema->lexema, "break")) {
                            lexema->lexemaToken = BREAK;
                        } else if (!strcmp(lexema->lexema, "continue")) {
                            lexema->lexemaToken = CONTINUE;
                        } else if (!strcmp(lexema->lexema, "return")) {
                            lexema->lexemaToken = RETURN;
                        } else if (!strcmp(lexema->lexema, "void")) {
                            lexema->lexemaToken = VOID;
                        } else if (!strcmp(lexema->lexema, "int")) {
                            lexema->lexemaToken = INT;
                        } else if (!strcmp(lexema->lexema, "float")) {
                            lexema->lexemaToken = FLOAT;
                        } else if (!strcmp(lexema->lexema, "char")) {
                            lexema->lexemaToken = CHAR;
                        } else if (!strcmp(lexema->lexema, "double")) {
                            lexema->lexemaToken = DOUBLE;
                        } else if (!strcmp(lexema->lexema, "long")) {
                            lexema->lexemaToken = LONG;
                        } else if (!strcmp(lexema->lexema, "short")) {
                            lexema->lexemaToken = SHORT;
                        } else if (!strcmp(lexema->lexema, "enum")) {
                            lexema->lexemaToken = ENUM;
                        } else if (!strcmp(lexema->lexema, "extern")) {
                            lexema->lexemaToken = EXTERN;
                        } else if (!strcmp(lexema->lexema, "goto")) {
                            lexema->lexemaToken = GOTO;
                        } else if (!strcmp(lexema->lexema, "register")) {
                            lexema->lexemaToken = REGISTER;
                        } else if (!strcmp(lexema->lexema, "signed")) {
                            lexema->lexemaToken = SIGNED;
                        } else if (!strcmp(lexema->lexema, "unsigned")) {
                            lexema->lexemaToken = UNSIGNED;
                        } else if (!strcmp(lexema->lexema, "static")) {
                            lexema->lexemaToken = STATIC;
                        } else if (!strcmp(lexema->lexema, "struct")) {
                            lexema->lexemaToken = STRUCT;
                        } else if (!strcmp(lexema->lexema, "sizeof")) {
                            lexema->lexemaToken = SIZEOF;
                        } else if (!strcmp(lexema->lexema, "switch")) {
                            lexema->lexemaToken = SWITCH;
                        } else if (!strcmp(lexema->lexema, "case")) {
                            lexema->lexemaToken = CASE;
                        } else if (!strcmp(lexema->lexema, "default")) {
                            lexema->lexemaToken = DEFAULT;
                        } else if (!strcmp(lexema->lexema, "const")) {
                            lexema->lexemaToken = CONST;
                        } else if (!strcmp(lexema->lexema, "volatile")) {
                            lexema->lexemaToken = VOLATILE;
                        } else if (!strcmp(lexema->lexema, "typedef")) {
                            lexema->lexemaToken = TYPEDEF;
                        } else if (!strcmp(lexema->lexema, "include")) {
                            lexema->lexemaToken = INCLUDE;
                        } else {
                            lexema->lexemaToken = ID;
                        }
                        
                        printf("Linha: %d, Lexema: %s, Token: %d\n", buffer->posFile, lexema->lexema, lexema->lexemaToken);
                    }

                    strcpy(lexema->lexema, " ");
                    lexema->posLexema = -1;
                    lexema->lexemaToken = 0;

                }
                
                if (isDigit(caracter)){
                    lexema->posLexema++;
                    lexema->lexema[lexema->posLexema] = caracter;

                    while (isDigit(caracter = get_next_char(buffer))) {
                        lexema->posLexema++;
                        lexema->lexema[lexema->posLexema] = caracter;
                    }

                    if (lexema->posLexema < 63) {
                        lexema->lexema[lexema->posLexema + 1] = '\0';
                    }

                    if (lexema->posLexema > -1) {
                        lexema->lexemaToken = NUM;
                        printf("Linha: %d, Lexema: %s, Token: %d\n", buffer->posFile, lexema->lexema, lexema->lexemaToken);
                    }
                    

                    strcpy(lexema->lexema, " ");
                    lexema->posLexema = -1;
                }
            }
            deallocate_buffer(buffer);
            deallocate_lexema(lexema);
        }
        fclose(source);
    }

    return 0;
}