#include "global.h"
#include "funcs.h"
#include "scan.h"
#include "util.h"

int Error = FALSE;

StateType transitionTable[MAXSTATE-1][CHARACTERTYPE] = {{INCOMENT, INERROR, INNUM, INID, INATRIB, DONE, DONE, DONE, DONE, INERROR},
                                                        {INCOMENT, START, INCOMENT, INCOMENT, INCOMENT, INCOMENT, INCOMENT, INCOMENT, INERROR, INCOMENT},
                                                        {DONE, DONE, INNUM, DONE, DONE, DONE, DONE, DONE, DONE, DONE},
                                                        {DONE, DONE, DONE, INID, DONE, DONE, DONE, DONE, DONE, DONE},
                                                        {INERROR, INERROR, INERROR, INERROR, INERROR, DONE, INERROR, INERROR, INERROR, INERROR}};

int advenceTable[MAXSTATE-1][CHARACTERTYPE] = { {TRUE, FALSE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE},
                                                {TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE},
                                                {FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE},
                                                {FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE},
                                                {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE}};

int acceptationTable[8] = {FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE};

int is_alpha(char caracter)
{
    return (caracter >= 'a' && caracter <= 'z') || (caracter >= 'A' && caracter <= 'Z');
}

int is_digit(char caracter)
{
    return (caracter >= '0' && caracter <= '9');
}

int is_comands(char caracter)
{
    return (caracter >= 0 && caracter <= 32);
}

int is_operator(char caracter)
{
    return (caracter == '+' || caracter == '-' || caracter == '*' || caracter == '/' || caracter == '<' || caracter == '(' || caracter == ')' || caracter == ';');
}

int is_left_brace(char caracter)
{
    return (caracter == '{');
}

int is_right_braces(char caracter)
{
    return (caracter == '}');
}

int is_two_points(char caracter)
{
    return (caracter == ':');
}

int is_equals(char caracter)
{
    return (caracter == '=');
}

int get_character_type(char caracter)
{
    if (is_left_brace(caracter)) {
        return 0;
    } else if (is_right_braces(caracter)) {
        return 1;
    } else if (is_digit(caracter)) {
        return 2;
    } else if (is_alpha(caracter)) {
        return 3;
    } else if (is_two_points(caracter)) {
        return 4;
    } else if (is_equals(caracter)) {
        return 5;
    } else if (is_operator(caracter)) {
        return 6;
    } else if (is_comands(caracter)) {
        return 7;
    } else if (caracter == EOF) {
        return 8;
    } else {
        return 9;
    }
}

int get_token_reserved_word(char * lexema)
{
    if (!strcmp(lexema, "if")) {
        return IF;
    } else if (!strcmp(lexema, "then")) {
        return THEN;
    } else if (!strcmp(lexema, "else")) {
        return ELSE;
    } else if (!strcmp(lexema, "end")) {
        return END;
    } else if (!strcmp(lexema, "repeat")) {
        return REPEAT;
    } else if (!strcmp(lexema, "until")) {
        return UNTIL;
    } else if (!strcmp(lexema, "read")) {
        return READ;
    } else if (!strcmp(lexema, "write")) {
        return WRITE;
    } else {
        return ID;
    }
}

int get_token_operation(char * lexema)
{
    if (!strcmp(lexema, "-")) {
        return MINUS;
    } else if (!strcmp(lexema, "*")) {
        return MULT;
    } else if (!strcmp(lexema, "/")) {
        return DIV;
    } else if (!strcmp(lexema, "<")) {
        return LT;
    } else if (!strcmp(lexema, "(")) {
        return LPAREN;
    } else if (!strcmp(lexema, ")")) {
        return RPAREN;
    } else if (!strcmp(lexema, ";")) {
        return SEMI;
    } else {
        return PLUS;
    }
}

int get_token(bufferStruct * buffer, lexemaStruct * lexema)
{
        
    StateType state = START;
    StateType newState;
    char character = get_next_char(buffer);
    int characterType;
    int canPrint = TRUE;

    while ((!acceptationTable[state]) && (state != INERROR)) {
        characterType = get_character_type(character);
        newState = transitionTable[state][characterType];

        if (state != INCOMENT) {
            lexema->posLexema++;
            lexema->lexema[lexema->posLexema] = character;
            if (newState == INID) {
                lexema->lexemaToken = ID;
                lexema->line = buffer->posFile;
            } else if ((state == INID) && (characterType != 3)) {
                lexema->posLexema--;
                unget_char(buffer);
            } else if (newState == INNUM) {
                lexema->lexemaToken = NUM;
                lexema->line = buffer->posFile;
            } else if ((state == INNUM) && (characterType != 2)) {
                lexema->posLexema--;
                unget_char(buffer);
            } else if ((characterType == 5) && (state == INATRIB)) {
                lexema->lexemaToken = ASSIGN;
                lexema->line = buffer->posFile;
            } else if (characterType == 5) {
                lexema->lexemaToken = EQ;
                lexema->line = buffer->posFile;
            } else if (characterType == 6) {
                lexema->lexemaToken = PLUS;
                lexema->line = buffer->posFile;
            } else if (characterType == 7) {
                canPrint = FALSE;
            } else if (characterType == 8) {
                lexema->lexemaToken = ENDFILE;
                lexema->line = buffer->posFile;
            }
        } else {
            strncpy(lexema->lexema, " ", 1);
            lexema->posLexema = -1;

            if (characterType == 8) {
                lexema->lexemaToken = ENDFILE;
                lexema->line = buffer->posFile;
                canPrint = FALSE;
                printf("%s: %d: ERRO: comentario nao terminado\n", currentFileName, lexema->line);
            }
        }

        if (advenceTable[state][characterType]) {
            character = get_next_char(buffer);
        }
        
        state = newState;
    }

    lexema->posLexema++;
    lexema->lexema[lexema->posLexema] = '\0';

    if ((!acceptationTable[state]) && (state == INERROR)) {
        lexema->lexemaToken = ERRO;
    } 

    if (lexema->lexemaToken == ID) {
        lexema->lexemaToken = get_token_reserved_word(lexema->lexema);
    }
    
    if (lexema->lexemaToken == PLUS) {
        lexema->lexemaToken = get_token_operation(lexema->lexema);
    }

    if (canPrint) {
        printf("%3d: ", lexema->line);
        print_token(lexema->lexemaToken, lexema->lexema);
    }

    strncpy(lexema->lexema, " ", 1);
    lexema->posLexema = -1;

    return lexema->lexemaToken;
}