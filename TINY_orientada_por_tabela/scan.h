#ifndef _SCAN_H_
#define _SCAN_H_

int is_alpha(char caracter);

int is_digit(char caracter);

int is_comands(char caracter);

int is_operator(char caracter);

int is_left_brace(char caracter);

int is_right_braces(char caracter);

int is_two_points(char caracter);

int is_equals(char caracter);

int get_character_type(char caracter);

int get_token_reserved_word(char * lexema);

int get_token_operation(char * lexama);

int get_token(bufferStruct * buffer, lexemaStruct * lexema);

#endif