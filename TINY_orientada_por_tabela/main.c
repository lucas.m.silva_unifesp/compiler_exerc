#include "global.h"
#include "funcs.h"
#include "scan.h"

FILE * source;

char * currentFileName;

TokenType token;

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        exit(1);
    }

    currentFileName = argv[1];

    source = fopen(argv[1], "r");

    if (source == NULL) {
        printf("Erro ao alocar memoria para o arquivo texto\n");
        exit(1);
    }

    bufferStruct * buffer = allocate_buffer();
    lexemaStruct * lexema = allocate_lexema();

    if (buffer == NULL || lexema == NULL) {
        printf("Memoria insuficiente para alocacao do buffer\n");
        exit(1);
    }
    else {
        while ((token = get_token(buffer, lexema)) != ENDFILE) {}

        deallocate_buffer(buffer);
        deallocate_lexema(lexema);
    }
    fclose(source);

    return 0;
}