%option noyywrap yylineno
%x COMMENT
%{
        #include "global.h"
        #include "scan.h"
        #include "util.h"

        TokenType currentToken;
%}

small_letter [a-z]
capital_letter [A-Z]
letter ({small_letter}|{capital_letter})
identifier {letter}+
digit [0-9]
number {digit}+
new_line \n
white_space [ \t]+

%%

"{"                     {BEGIN COMMENT;}
<COMMENT>"}"            {BEGIN INITIAL;}
<COMMENT>\n             {}
<COMMENT>.
<COMMENT><<EOF>>        {printf("%s:%d: Unterminated comment\n", currentFileName, yylineno); return ENDFILE;}


"if"                    {return IF;}
"then"                  {return THEN;}
"else"                  {return ELSE;}
"end"                   {return END;}
"repeat"                {return REPEAT;}
"until"                 {return UNTIL;}
"read"                  {return READ;}
"write"                 {return WRITE;}
"+"                     {return PLUS;}
"-"                     {return MINUS;}
"*"                     {return MULT;}
"/"                     {return DIV;}
"="                     {return EQ;}
"<"                     {return LT;}
"("                     {return LPAREN;}
")"                     {return RPAREN;}
";"                     {return SEMI;}
":="                    {return ASSIGN;}
{identifier}            {return ID;}
{number}                {return NUM;}
{new_line}              {}
{white_space}           {}
.                       {}
<<EOF>>                 {return ENDFILE;}

%%
// code

TokenType get_token()
{
        static int firsTime = TRUE;
        if (firsTime) {
                firsTime = FALSE;
                yyin = source;
        }

        currentToken = yylex();

        printf("%d:", yylineno);
        print_token(currentToken, yytext);

        return currentToken;
}