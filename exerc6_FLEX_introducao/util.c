#include "global.h"
#include "util.h"

void print_token(int tokenType, char * lexema) 
{
        switch (tokenType) {
                case IF:
                        printf(" Lexema = %s; Token = IF\n", lexema);
                        break;
                case THEN:
                        printf(" Lexema = %s; Token = THEN\n", lexema);
                        break;
                case ELSE:
                        printf(" Lexema = %s; Token = ELSE\n", lexema);
                        break;
                case END:
                        printf(" Lexema = %s; Token = END\n", lexema);
                        break;
                case REPEAT:
                        printf(" Lexema = %s; Token = REPEAT\n", lexema);
                        break;
                case UNTIL:
                        printf(" Lexema = %s; Token = UNTIL\n", lexema);
                        break;
                case READ:
                        printf(" Lexema = %s; Token = READ\n", lexema);
                        break;
                case WRITE:
                        printf(" Lexema = %s; Token = WRITE\n", lexema);
                        break;
                case ID:
                        printf(" Lexema = %s; Token = ID\n", lexema);
                        break;
                case NUM:
                        printf(" Lexema = %s; Token = NUM\n", lexema);
                        break;
                case PLUS:
                        printf(" Lexema = %s; Token = PLUS\n", lexema);
                        break;
                case MINUS:
                        printf(" Lexema = %s; Token = MINUS\n", lexema);
                        break;
                case MULT:
                        printf(" Lexema = %s; Token = MULT\n", lexema);
                        break;
                case DIV:
                        printf(" Lexema = %s; Token = DIV\n", lexema);
                        break;
                case EQ:
                        printf(" Lexema = %s; Token = EQ\n", lexema);
                        break;
                case LT:
                        printf(" Lexema = %s; Token = LT\n", lexema);
                        break;
                case LPAREN:
                        printf(" Lexema = %s; Token = LPAREN\n", lexema);
                        break;
                case RPAREN:
                        printf(" Lexema = %s; Token = RPAREN\n", lexema);
                        break;
                case ASSIGN:
                        printf(" Lexema = %s; Token = ASSIGN\n", lexema);
                        break;
                case SEMI:
                        printf(" Lexema = %s; Token = SEMI\n", lexema);
                        break;
                case ENDFILE:
                        printf(" Token = ENDFILE\n");
                        break;
                case ERRO:
                        printf("ERRO\n");
                        break;
                default:
                        break;
        }
}