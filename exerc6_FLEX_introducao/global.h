#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// quantidade de caracteres que o buffer suporta
#define bufferSize 64

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

// tokens
typedef enum {
    IF = 300,
    THEN = 301,
    ELSE = 302,
    END = 303,
    REPEAT = 304,
    UNTIL = 305,
    READ = 306,
    WRITE = 307,
    ID = 308,
    NUM = 309,
    INT = 310,
    BOOL = 311,
    PLUS = 312,
    MINUS = 313,
    MULT = 314,
    DIV = 315,
    EQ = 316,
    LT = 317,
    LPAREN = 318,
    RPAREN = 319,
    ASSIGN = 320,
    SEMI = 321,
    ENDFILE = 322,

    ERRO = 400
} TokenType;

// ponteiro para o arquivo texto
extern FILE * source;

// nome do arquivo
extern char * currentFileName;

// token da ultima leitura
extern TokenType token;

// flag de erro
extern int Error;

#endif // _GLOBAL_H_