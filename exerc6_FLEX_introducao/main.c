#include "global.h"
#include "scan.h"

FILE * source;

char * currentFileName;

TokenType token;

int main(int argc, char * argv[])
{
    if (argc < 2) {
        perror("Falta de parametro!!! Arquivo ausente.");
        exit(1);
    }

    if (argc > 2) {
        perror("Excesso de parametros!!!");
        exit(1);
    }

    currentFileName = argv[1];

    if (!(source = fopen(argv[1], "r"))) {
        perror(argv[0]);
        exit(1);
    }


    while ((token = get_token()) != ENDFILE);

    fclose(source);

    return 0;
}