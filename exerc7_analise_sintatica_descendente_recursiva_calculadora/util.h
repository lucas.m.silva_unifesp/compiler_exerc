#ifndef _UTIL_H_
#define _UTIL_H_

#include "global.h"

void print_token(TokenType tokenType, char * lexema);

TreeNode * new_exp_node(ExpKind nodeKind);

void free_exp_node_tree(TreeNode * node);

int result(TreeNode * node);

void print_tree(TreeNode * node);

#endif // _UTIL_H_