%option noyywrap yylineno

%{
        #include "global.h"
        #include "scan.h"
        #include "util.h"

        TokenType currentToken;

        char tokenString[MAXLEXEMALENTH + 1];
%}

digit [0-9]
number {digit}+
new_line \n
white_space [ \t]+

%%

"+"                     {return PLUS;}
"-"                     {return MINUS;}
"*"                     {return TIMES;}
"/"                     {return OVER;}
"("                     {return LPAREN;}
")"                     {return RPAREN;}
{number}                {return NUM;}
{new_line}              {}
{white_space}           {}
.                       {}
<<EOF>>                 {return ENDFILE;}

%%
// code

TokenType get_token()
{
        static int firsTime = TRUE;
        if (firsTime) {
                firsTime = FALSE;
                yyin = source;
        }

        currentToken = yylex();
        strncpy(tokenString, yytext, MAXLEXEMALENTH);

        printf("%d:", yylineno);
        print_token(currentToken, yytext);

        return currentToken;
}