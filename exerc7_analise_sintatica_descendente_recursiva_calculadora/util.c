#include "util.h"

static void print_token_type(TokenType tokenType)
{
        switch (tokenType) {
                case NUM:
                        printf(" NUM \n");
                        break;
                case PLUS:
                        printf(" PLUS \n");
                        break;
                case MINUS:
                        printf(" MINUS \n");
                        break;
                case TIMES:
                        printf(" TIMES \n");
                        break;
                case OVER:
                        printf(" OVER \n");
                        break;
                case LPAREN:
                        printf(" LPAREN \n");
                        break;
                case RPAREN:
                        printf(" RPAREN \n");
                        break;
                case ENDFILE:
                        printf(" ENDFILE \n");
                        break;
                case ERRO:
                        printf(" ERRO \n");
                        break;
                default:
                        break;
        }
}

void print_token(TokenType tokenType, char * lexema) 
{
        if ((tokenType != ENDFILE) && (tokenType != ERRO)) {
                printf(" Lexema = %s |", lexema);
        } 

        printf(" Token = ");
        print_token_type(tokenType);
}

TreeNode * new_exp_node(ExpKind nodeKind)
{
        TreeNode * node = (TreeNode *) malloc(sizeof(TreeNode));

        int i;

        if (node == NULL) {
                printf("Out of memory error\n");
                exit(1);
        }

        for (i = 0; i < MAXCHILDREN; i++) {
                node->child[i] = NULL;
        }

        node->sibling = NULL;
        node->nodeKind = ExpK;
        node->kind.exp = nodeKind;

        return node;
}

void free_exp_node_tree(TreeNode * node)
{
        int childIndex;

        for (childIndex = 0; childIndex < MAXCHILDREN; childIndex++) {
                if (node->child[childIndex] != NULL) {
                        free_exp_node_tree(node->child[childIndex]);
                }

                if (node->sibling != NULL) {
                        free_exp_node_tree(node->sibling);
                }
        }      
        free(node);
}

int result(TreeNode * node)
{
        int childIndex;

        for (childIndex = 0; childIndex < MAXCHILDREN; childIndex++) {
                if (node->child[childIndex] != NULL) {
                        result(node->child[childIndex]);
                }
        }    

        if (node->kind.exp == ConstK) {
                return node->atribute.value;
        } else if (node->kind.exp == OpK) {
                switch (node->atribute.operation) {
                        case PLUS:
                                return result(node->child[0]) + result(node->child[1]);
                                break;
                        case MINUS:
                                return result(node->child[0]) - result(node->child[1]);
                                break;
                        case TIMES:
                                return result(node->child[0]) * result(node->child[1]);
                                break;
                        case OVER:
                                return result(node->child[0]) / result(node->child[1]);
                                break;
                        default:
                                break;
                }
        }
}

static int indentation = 0;

#define INDENT indentation+=2
#define UNINDENT indentation-=2

static void print_spaces(void)
{ int inden;
  for (inden=0;inden<indentation;inden++)
    printf(" ");
}

void print_tree (TreeNode * node)
{
  int childIndex;
  INDENT;

  while (node != NULL) {
    print_spaces();
    if (node->nodeKind ==  ExpK) {
      switch (node->kind.exp) {
        case OpK:       printf("Op:"); 
                        print_token_type(node->atribute.operation);
                        break;
        case ConstK:    printf("const: %d\n", node->atribute.value); 
                        break;
        default:        printf("Unknown ExpNode kind\n"); 
                        break;
      }
    }
    else
      printf("Unknown node kind\n");
    
    for (childIndex=0; childIndex<MAXCHILDREN; childIndex++)
      print_tree(node->child[childIndex]);
    
    node = node->sibling;
  }

  UNINDENT;
}