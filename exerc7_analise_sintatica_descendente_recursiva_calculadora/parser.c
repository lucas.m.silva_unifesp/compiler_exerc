#include "parser.h"

TokenType token;

void error(void)
{
        printf("ERRO\n");
        exit(1);
}

void match(TokenType expectedToken)
{
        if (token == expectedToken) {
                token = get_token();
        } else {
                error();
        }
}


TreeNode * expr(void)
{
        TreeNode * expression;
        TreeNode * newExpression;

        expression = termo();

        while ((token == PLUS) || (token == MINUS)) {
                newExpression = new_exp_node(OpK);
                if (token == PLUS) {
                        newExpression->atribute.operation = PLUS;
                        match(PLUS);
                        newExpression->child[0] = expression;
                        newExpression->child[1] = termo();
                } else {
                        newExpression->atribute.operation = MINUS;
                        match(MINUS);
                        newExpression->child[0] = expression;
                        newExpression->child[1] = termo();
                }

                expression = newExpression;
        }

        return expression;
}

TreeNode * termo(void)
{
        TreeNode * expression;
        TreeNode * newExpression;

        expression = fator();

        while ((token == TIMES) || (token == OVER)) {
                newExpression = new_exp_node(OpK);
                if (token == TIMES) {
                        newExpression->atribute.operation = TIMES;
                        match(TIMES);
                        newExpression->child[0] = expression;
                        newExpression->child[1] = fator();
                } else {
                        newExpression->atribute.operation = OVER;
                        match(OVER);
                        newExpression->child[0] = expression;
                        newExpression->child[1] = fator();
                }

                expression = newExpression;
        }

        return expression;
}

TreeNode * fator(void)
{
        TreeNode * expression;

        switch (token) {
                case LPAREN:
                        match(LPAREN);
                        expression = expr();
                        match(RPAREN);
                        break;
                case NUM:
                        expression = new_exp_node(ConstK);
                        expression->atribute.value = atoi(tokenString);
                        match(NUM);
                        break;
                case ENDFILE:
                        expression = NULL;
                        break;
                default:
                        error();
                        break;
        }

        return expression;
}


