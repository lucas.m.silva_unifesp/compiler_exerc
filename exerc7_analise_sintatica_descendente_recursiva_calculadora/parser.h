#ifndef _PARSER_H_
#define _PARSER_H_

#include "global.h"
#include "scan.h"
#include "util.h"

TreeNode * expr(void);
TreeNode * termo(void);
TreeNode * fator(void);

void error(void);

void match(TokenType expectedToken);

#endif