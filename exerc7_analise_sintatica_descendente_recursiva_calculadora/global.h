#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

//tamanho maximo de filhos
#define MAXCHILDREN 2

// tokens
typedef enum {
    NUM = 300,
    PLUS = 301,
    MINUS = 302,
    TIMES = 303,
    OVER = 304,
    LPAREN = 305,
    RPAREN = 306,
    ENDFILE = 307,

    ERRO = 400
} TokenType;

typedef enum {
    ExpK
} NodeKind;

typedef enum {
    OpK, ConstK
} ExpKind;

typedef enum {
    Integer
} ExpType;

// estrutura dos nos da arvore sintatica
typedef struct TreeNode {
    struct TreeNode * child[MAXCHILDREN];
    struct TreeNode * sibling;
    NodeKind nodeKind;

    union {
        ExpKind exp;
    } kind;
    
    union {
        TokenType operation;
        int value;
    } atribute;

} TreeNode;

// ponteiro para o arquivo texto
extern FILE * source;

// nome do arquivo
extern char * currentFileName;

extern char tokenString[MAXLEXEMALENTH + 1];

// token da ultima leitura
extern TokenType token;

// flag de erro
extern int Error;

#endif // _GLOBAL_H_