#include "global.h"
#include "scan.h"
#include "parser.h"

FILE * source;

char * currentFileName;

int main(int argc, char * argv[])
{
    if (argc < 2) {
        perror("Falta de parametro!!! Arquivo ausente.");
        exit(1);
    }

    if (argc > 2) {
        perror("Excesso de parametros!!!");
        exit(1);
    }

    currentFileName = argv[1];

    if (!(source = fopen(argv[1], "r"))) {
        perror(argv[0]);
        exit(1);
    }

    TreeNode * sintaxTree;

    token = get_token();

    sintaxTree = expr();

    printf("O valor calculado é: %d\n", result(sintaxTree));

    printf("\nArvore Sintatica:\n");
    print_tree(sintaxTree);

    free_exp_node_tree(sintaxTree);

    fclose(source);

    return 0;
}