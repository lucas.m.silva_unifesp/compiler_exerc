#include "funcs.h"

struct bufferStruct * allocate_buffer (int buffer_size)
{
    struct bufferStruct * buffer = (struct bufferStruct *) malloc (sizeof(struct bufferStruct));

    buffer->qtdBuffer = 0;
    buffer->posBuffer = 0;
    buffer->posArquivo = 1;

    return buffer;
}

char get_next_char(struct bufferStruct * buffer)
{
    if (buffer->qtdBuffer == buffer->posBuffer || buffer->bufferCarac[buffer->posBuffer] == '\0') {
        if (!feof(source)) {
            fgets(buffer->bufferCarac, tamBuffer, source);
        } else 
            buffer->bufferCarac[0] = EOF;
        
        buffer->qtdBuffer = strlen(buffer->bufferCarac);
        buffer->posBuffer = -1;
    }
    
    buffer->posBuffer++;

    if (buffer->bufferCarac[buffer->posBuffer] == '\n')
        buffer->posArquivo++;

    return buffer->bufferCarac[buffer->posBuffer];
}

void deallocate_buffer(struct bufferStruct * buffer)
{
    if (buffer != NULL)
        free(buffer);
}