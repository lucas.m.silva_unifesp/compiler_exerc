#ifndef _FUNCS_H_
#define _FUNCS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// quantidade de caracteres que o buffer suporta
#define tamBuffer 64

// strutura do buffer
struct bufferStruct
{
    char bufferCarac[tamBuffer]; // armazenamento dos caracteres
    int qtdBuffer; // quantidade de caracteres dentro do buffer
    int posBuffer; // posicao do caracter dentro do buffer
    int posArquivo; // posicao da linha do arquivo
};

// ponteiro para o arquivo texto
extern FILE * source;

/*
    funcao de alocacao do buffer
*/
struct bufferStruct * allocate_buffer();

/*
    funcao captura uma string do arquivo
    armazenando no buffer, posteriormente
    sera mandado caracter por caracter do buffer
    ate que nao tenha mais nada no buffer para 
    ser mandado
*/
char get_next_char(struct bufferStruct * buffer);

/*
    funcao de deslocamento do buffer
*/
void deallocate_buffer(struct bufferStruct * buffer);

#endif