#include "funcs.h"

int Error = FALSE;

FILE * source;

int buffer_size;

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        Error = TRUE;
    }

    if (!Error) {
        source = fopen(argv[1], "r");

        if (source == NULL) {
            printf("Erro ao alocar memoria para o arquivo texto\n");
            Error = FALSE;
        }
    }

    if (!Error) {
        struct bufferStruct * buffer = allocate_buffer();

        if (buffer == NULL) {
            printf("Memoria insuficiente para alocacao do buffer");
            exit(1);
        }
        else {
            char caracter;

            while ((caracter = get_next_char(buffer)) != EOF) {          
                if (caracter > 125) {
                    printf("\n\nErro lexico na linha: %d\n", buffer->posArquivo);
                    printf("Caracter nao utilizado na computacao\n");
                    printf("Caracter: %c\n", caracter);
                    deallocate_buffer(buffer);
                    fclose(source);
                    exit(1);
                }

                if (caracter != EOF)
                    printf("%c", caracter);
            }

            printf("\n%d\n", buffer->posArquivo);

            deallocate_buffer(buffer);
        }

        fclose(source);
    }

    return 0;
}