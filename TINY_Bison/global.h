#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef YYPARSER

#include "tiny.tab.h"

/*
    ENDFILE eh definido implicitamente por Yacc/Bison
    e nao eh incluido arquivo tab.h
*/
#define ENDFILE 0

#endif

// quantidade de caracteres que o buffer suporta
#define bufferSize 64

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

#define MAXSTATE 6
#define CHARACTERTYPE 10

// tokens
// typedef enum {
//     IF = 300,
//     THEN = 301,
//     ELSE = 302,
//     END = 303,
//     REPEAT = 304,
//     UNTIL = 305,
//     READ = 306,
//     WRITE = 307,
//     ID = 308,
//     NUM = 309,
//     INT = 310,
//     BOOL = 311,
//     PLUS = 312,
//     MINUS = 313,
//     TIMES = 314,
//     OVER = 315,
//     EQ = 316,
//     LT = 317,
//     LPAREN = 318,
//     RPAREN = 319,
//     ASSIGN = 320,
//     SEMI = 321,
//     ENDFILE = 322,

//     NONE = 399,
//     ERRO = 400
// } TokenType;

typedef int TokenType;

// estados
typedef enum {
    START = 0, 
    INCOMENT = 1, 
    INNUM = 2, 
    INID = 3, 
    INATRIB = 4, 
    DONE = 5,
    INERROR = 6
} StateType;

// strutura do buffer
typedef struct bufferBuild
{
    char bufferCarac[bufferSize]; // armazenamento dos caracteres
    int qtdBuffer; // quantidade de caracteres dentro do buffer
    int posBuffer; // posicao do caracter dentro do buffer
    int posFile; // posicao da linha do arquivo
} bufferStruct;

// strutura do lexama e do token
typedef struct lexemaBuild
{
    char lexema[MAXLEXEMALENTH]; // armazenamento do lexema
    int posLexema; // posicao do lexema
    int lexemaToken; // token do lexema
    int line; // linha do arquivo que aparece o lexema
} lexemaStruct;

/************************************************************/
/******** Árvore sintática para a análise sintática ********/
/************************************************************/ 

#define MAXCHILDREN 3

typedef enum {
    StmtK,ExpK
} NodeKind; 

typedef enum {
    IfK,RepeatK,AssignK,ReadK,WriteK
} StmtKind; 

typedef enum {
    OpK,ConstK,IdK
} ExpKind;

typedef enum {
    Void,Integer,Boolean
} ExpType;

typedef struct treeNode {
    struct treeNode * child[MAXCHILDREN];
    struct treeNode * sibling;
    int line;
    NodeKind nodekind;

    union {
        StmtKind stmt;
        ExpKind exp;
    } kind;

    union {
        TokenType op;
        int val;
        char * name;
    } attr;
    ExpType type;
} TreeNode;

// ponteiro para o arquivo texto
extern FILE * source;

// nome do arquivo
extern char * currentFileName;

// token da ultima leitura
extern TokenType token;

extern bufferStruct * buffer;

extern lexemaStruct * lexema;

// flag de erro
extern int Error;

#endif // _GLOBAL_H_