#include "global.h"
#include "funcs.h"
#include "scan.h"
#include "parse.h"
#include "util.h"

FILE * source;

char * currentFileName;

TokenType token;

bufferStruct * buffer;

lexemaStruct * lexema;

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        exit(1);
    }

    currentFileName = argv[1];

    source = fopen(argv[1], "r");

    if (source == NULL) {
        printf("Erro ao alocar memoria para o arquivo texto\n");
        exit(1);
    }

    buffer = allocate_buffer();
    lexema = allocate_lexema();

    if (buffer == NULL || lexema == NULL) {
        printf("Memoria insuficiente para alocacao do buffer\n");
        exit(1);
    }
    else {
        // get_token(buffer, lexema);

        // while (lexema->lexemaToken != ENDFILE) {
        //     get_token(buffer, lexema);
        // }

        TreeNode * syntaxTree;
        syntaxTree = parse();

        print_tree(syntaxTree);


        deallocate_buffer(buffer);
        deallocate_lexema(lexema);
    }
    fclose(source);

    return 0;
}