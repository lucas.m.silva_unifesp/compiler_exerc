#include "global.h"
#include "util.h"

void print_token_type(TokenType tokenType) 
{
        switch (tokenType) {
                case IF:
                        printf("IF\n");
                        break;
                case THEN:
                        printf("THEN\n");
                        break;
                case ELSE:
                        printf("ELSE\n");
                        break;
                case END:
                        printf("END\n");
                        break;
                case REPEAT:
                        printf("REPEAT\n");
                        break;
                case UNTIL:
                        printf("UNTIL\n");
                        break;
                case READ:
                        printf("READ\n");
                        break;
                case WRITE:
                        printf("WRITE\n");
                        break;
                case ID:
                        printf("ID\n");
                        break;
                case NUM:
                        printf("NUM\n");
                        break;
                case PLUS:
                        printf("PLUS\n");
                        break;
                case MINUS:
                        printf("MINUS\n");
                        break;
                case TIMES:
                        printf("TIMES\n");
                        break;
                case OVER:
                        printf("OVER\n");
                        break;
                case EQ:
                        printf("EQ\n");
                        break;
                case LT:
                        printf("LT\n");
                        break;
                case LPAREN:
                        printf("LPAREN\n");
                        break;
                case RPAREN:
                        printf("RPAREN\n");
                        break;
                case ASSIGN:
                        printf("ASSIGN\n");
                        break;
                case SEMI:
                        printf("SEMI\n");
                        break;
                default:
                        break;
        }
}

void print_token(TokenType tokenType, char * lexema) 
{
        if (!(tokenType == ERRO) && !(tokenType == ENDFILE)) {
                printf(" Lexema = %-10s | Token = ", lexema);
                print_token_type(tokenType);
        } else if (tokenType == ENDFILE) {
                printf(" Token = ENDFILE\n");
        } else if (tokenType == ERRO) {
                printf(" Lexema = %-10s; Token = ERRO\n", lexema);
        }
}


TreeNode * new_stmt_node(StmtKind kind, int line)
{ 
        TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
        int i; 
        if (t==NULL) 
                printf("Out of memory error at line %d\n", line); 
        else {
                for (i=0;i<MAXCHILDREN;i++) 
                        t->child[i] = NULL; 
                
                t->sibling = NULL;
                t->nodekind = StmtK; 
                t->kind.stmt = kind; 
                t->line = line;
        } 
        
        return t;
}

TreeNode * new_exp_node(ExpKind kind, int line) 
{ 
        TreeNode * t = (TreeNode *) malloc(sizeof(TreeNode));
        int i; 
        if (t==NULL) 
                printf("Out of memory error at line %d\n", line); 
        else {
                for (i=0;i<MAXCHILDREN;i++) 
                        t->child[i] = NULL; 

                t->sibling = NULL; 
                t->nodekind = ExpK; 
                t->kind.exp = kind; 
                t->line = line; 
                t->type = Void;
        } 
        
        return t;
}

char * copy_string(char * s, int line) 
{ 
        int n; 
        char * t;
        if (s==NULL)    
                return NULL; 
        
        n = strlen(s)+1; 
        t = malloc(n); 
        if (t==NULL)
                printf("Out of memory error at line %d\n", line); 
        else strcpy(t,s); 
                return t;
} 

static int indentno = 0;

#define INDENT indentno+=2
#define UNINDENT indentno-=2

static void print_spaces(void)
{ 
        int i;
        for (i=0; i<indentno; i++) 
                printf(" ");
}

void print_tree( TreeNode * tree )
{ 
        int i; 
        INDENT;
        while (tree != NULL) { 
                print_spaces();
                if (tree->nodekind==StmtK) {
                        switch (tree->kind.stmt) {
                                case IfK:
                                        printf("If\n"); 
                                        break;
                                case RepeatK:
                                        printf("Repeat\n"); 
                                        break;
                                case AssignK:
                                        printf("Assign to: %s\n",tree->attr.name); 
                                        break;
                                case ReadK:
                                        printf("Read: %s\n",tree->attr.name); 
                                        break;
                                case WriteK:
                                        printf("Write\n"); 
                                        break;
                                default:
                                        printf("Unknown ExpNode kind\n"); 
                                        break;
                                } 
                } else if (tree->nodekind==ExpK) { 
                        switch (tree->kind.exp) { 
                                case OpK:
                                        printf("Op: ");
                                        print_token_type(tree->attr.op); 
                                        break;
                                case ConstK:
                                        printf("const: %d\n",tree->attr.val); 
                                        break;
                                case IdK:
                                        printf("Id: %s\n",tree->attr.name); 
                                        break;
                                default:
                                        printf("Unknown ExpNode kind\n"); 
                                        break;
                        } 
                } else 
                        printf("Unknown node kind\n"); 
                
                for (i=0;i<MAXCHILDREN;i++)
                        print_tree(tree->child[i]); 
                
                tree = tree->sibling;
        } 
        
        UNINDENT; 
}