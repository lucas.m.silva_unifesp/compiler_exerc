#ifndef _UTIL_H_
#define _UTIL_H_

void print_token_type(TokenType);

void print_token(int, char * );

TreeNode * new_stmt_node(StmtKind, int );

TreeNode * new_exp_node(ExpKind, int);

char * copy_string( char *, int );

void print_tree(TreeNode * ); 

#endif // _UTIL_H_