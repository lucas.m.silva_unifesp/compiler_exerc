%{
#define YYPARSER /* diferencia saída Yacc de outros arquivos de código */ 
#include "global.h" 
#include "util.h"
#include "scan.h"
#include "parse.h" 

#define YYSTYPE TreeNode *
static YYSTYPE savedTree;  /* armazena arvore sintatica para reorno posterior*/

static char * savedName; /* para uso em atribuições */ 
static int savedLineNo; /* idem */ 
static int yylex(void);
static int yyerror(char *);
%} 

// %define api.value.type { TreeNode * }
%token IF THEN ELSE END REPEAT UNTIL READ WRITE 
%token ID NUM 
%token ASSIGN EQ LT PLUS MINUS TIMES OVER LPAREN RPAREN SEMI 
%token NONE
%token ENDFILE
%token ERRO

%% /* Gramática para TINY */

program         :        stmt_seq { savedTree = $1;} 
                ;
stmt_seq        :       stmt_seq SEMI stmt
                                {
                                        YYSTYPE t = $1;
                                        if (t != NULL) {
                                                while (t->sibling != NULL)
                                                        t = t->sibling;
                                                t->sibling = $3;
                                                $$ = $1;
                                        } else 
                                                $$ = $3;
                                }
                |       stmt { $$ = $1; } 
                ;
stmt            :       if_stmt { $$ = $1; } 
                |       repeat_stmt { $$ = $1; } 
                |       assign_stmt { $$ = $1; } 
                |       read_stmt { $$ = $1; } 
                |       write_stmt { $$ = $1; } 
                |       error { $$ = NULL; }
                ;
if_stmt         :       IF exp THEN stmt_seq END 
                                { 
                                        $$ = new_stmt_node(IfK, savedLineNo); 
                                        $$->child[0] = $2; 
                                        $$->child[1] = $4;
                                }
                |       IF exp THEN stmt_seq ELSE stmt_seq END 
                                { 
                                        $$ = new_stmt_node(IfK, savedLineNo); 
                                        $$->child[0] = $2; 
                                        $$->child[1] = $4; 
                                        $$->child[2] = $6;
                                }
                ;
repeat_stmt     :       REPEAT stmt_seq UNTIL exp 
                                { 
                                        $$ = new_stmt_node(RepeatK, savedLineNo);
                                        $$->child[0] = $2; 
                                        $$->child[1] = $4;
                                }
                ;
assign_stmt     :       ident ASSIGN exp 
                                {       
                                        $$ = $1; 
                                        $$->nodekind = StmtK;
                                        $$->kind.stmt = AssignK;
                                        $$->child[0] = $3;
                                        $$->line = savedLineNo;
                                }
                ;
read_stmt       :       READ ID
                                { 
                                        $$ = new_stmt_node(ReadK, savedLineNo); 
                                        $$->attr.name = copy_string(savedName, savedLineNo);
                                }
                ;
write_stmt      :       WRITE exp
                                {
                                        $$ = new_stmt_node(WriteK, savedLineNo); 
                                        $$->child[0] = $2;
                                }
                ;
exp             :       simple_exp LT simple_exp
                                {
                                        $$ = new_exp_node(OpK, savedLineNo); 
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3; 
                                        $$->attr.op = LT;
                                }
                |       simple_exp EQ simple_exp
                                {
                                        $$ = new_exp_node(OpK, savedLineNo); 
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3; 
                                        $$->attr.op = EQ;
                                }
                |       simple_exp { $$ = $1; }
                ;
simple_exp      :       simple_exp PLUS term
                                {
                                        $$ = new_exp_node(OpK, savedLineNo); 
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3;
                                        $$->attr.op = PLUS;
                                }
                |       simple_exp MINUS term
                                {
                                        $$ = new_exp_node(OpK, savedLineNo); 
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3;
                                        $$->attr.op = MINUS; 
                                }
                |       term { $$ = $1; }
                ;
term            :       term TIMES factor
                                {
                                        $$ = new_exp_node(OpK, savedLineNo); 
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3;
                                        $$->attr.op = TIMES;
                                }
                |       term OVER factor
                                {
                                        $$ = new_exp_node(OpK, savedLineNo);
                                        $$->child[0] = $1; 
                                        $$->child[1] = $3; 
                                        $$->attr.op = OVER;
                                }
                |       factor { $$ = $1; }
                ;
factor          :       LPAREN exp RPAREN { $$ = $1; }
                |       NUM
                                {
                                        $$ = new_exp_node(ConstK, savedLineNo); 
                                        $$->attr.val = atoi(savedName); 
                                }
                |       ident { $$ = $1; }
                |       error { $$ = NULL; }
                ;
ident           :       ID 
                                { 
                                        $$ = new_exp_node(IdK, savedLineNo); 
                                        $$->attr.name = copy_string(savedName, savedLineNo);
                                }

%%

int yyerror (char * message)
{
        printf("Syntax error at line %d: %s\n", savedLineNo, message); 
        printf("Current token: "); 
        print_token(yychar,savedName); 
        Error = TRUE; 

        return 0;
}

static int yylex(void)
{
        get_token(buffer, lexema);
        while (lexema->lexemaToken == NONE) {
                get_token(buffer, lexema);
        }

        savedLineNo = lexema->line;
        savedName = copy_string(lexema->lexema, savedLineNo);
        

        return lexema->lexemaToken;
}

TreeNode * parse(void)
{
        yyparse();
        return savedTree;
}




