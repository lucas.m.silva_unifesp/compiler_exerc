#include "funcs.h"

char * allocate_buffer (int buffer_size)
{
    char * buffer = (char *) malloc (sizeof(char)*buffer_size);

    return buffer;
}

void replace_print(char * buffer, int buffer_size)
{
    int i;

    int bufsize = 0;
    
    while ((buffer[bufsize] = fgetc(source)) != EOF) {
        
        if (buffer[bufsize]>96 && buffer[bufsize]<123)
            buffer[bufsize] = buffer[bufsize] - 32;
        else if (buffer[bufsize]>64 && buffer[bufsize]<91) 
            buffer[bufsize] = buffer[bufsize] + 32;
        
        bufsize++;

        if (bufsize == buffer_size-1) {
            buffer[bufsize] = '\0';

            printf("%s", buffer);

            bufsize = 0;
        }
    }

    buffer[bufsize] = '\0';

    printf("%s\n", buffer);
}

void deallocate_buffer(char * buffer)
{
    if (buffer != NULL)
        free(buffer);
}