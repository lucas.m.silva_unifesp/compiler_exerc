#ifndef  _FUNCS_H_
#define _FUNCS_H_

#include <stdio.h>
#include <stdlib.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

extern FILE * source;

char * allocate_buffer(int buffer_size);

void replace_print(char * buffer, int buffer_size);

void deallocate_buffer(char * buffer);

#endif