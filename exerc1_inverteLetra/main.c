#include "funcs.h"

int Error = FALSE;

FILE * source;

int buffer_size;

int main(int argc, char * argv[])
{
    if (argc != 3) {
        printf("Erro ao ler os parametros\n");
        Error = TRUE;
    }

    if (!Error) {
        source = fopen(argv[1], "r");

        buffer_size = atoi(argv[2]);

        char * buffer = allocate_buffer(buffer_size);

        replace_print(buffer, buffer_size);

        deallocate_buffer(buffer);

        fclose(source);
    }
}