#ifndef _SCAN_H_
#define _SCAN_H_

#define ID 300
#define IF 301
#define ELSE 302
#define WHILE 303
#define DO 304
#define FOR 305
#define BREAK 306
#define CONTINUE 307
#define RETURN 308
#define VOID 309
#define INT 310
#define FLOAT 311
#define CHAR 312
#define DOUBLE 313
#define LONG 314
#define SHORT 315
#define ENUM 316
#define EXTERN 317
#define GOTO 318
#define REGISTER 319
#define SIGNED 320
#define UNSIGNED 321
#define STATIC 322
#define STRUCT 323
#define SIZEOF 324
#define SWITCH 325
#define CASE 326
#define DEFAULT 327
#define CONST 328
#define VOLATILE 329
#define EQ 330
#define NE 331
#define ASSIGN 332
#define NUM 333
#define PLUS 334
#define MINUS 335
#define LBRACE 336
#define RBRACE 337
#define LPAREN 338
#define RPAREN 339
#define LBRACKET 340
#define RBRACKET 341
#define STRING 342
#define LT 343
#define BT 344
#define LTE 345
#define BTE 346
#define TWOPOINTS 347
#define STAR 348
#define TYPEDEF 349

#define PALAVRA_CHAVE 600

extern int isAlpha(char c);

extern int isDigit(char c);

int get_token();


#endif // _SCAN_H_