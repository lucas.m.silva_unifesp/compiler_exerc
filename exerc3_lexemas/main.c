#include "global.h"
#include "funcs.h"
#include "scan.h"

int Error = FALSE;

FILE * source;

int buffer_size;

int isAlpha(char c)
{
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int isDigit(char c)
{
        return (c >= '0' && c <= '9');
}

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        Error = TRUE;
    }

    if (!Error) {
        source = fopen(argv[1], "r");

        if (source == NULL) {
            printf("Erro ao alocar memoria para o arquivo texto\n");
            Error = FALSE;
        }
    }

    if (!Error) {
        bufferStruct * buffer = allocate_buffer();
        lexemaStruct * lexema = allocate_lexema();

        if (buffer == NULL || lexema == NULL) {
            printf("Memoria insuficiente para alocacao do buffer");
            exit(1);
        }
        else {
            char caracter;

            while ((caracter = get_next_char(buffer)) != EOF) {
                if (isAlpha(caracter)) {
                    lexema->posLexema++;
                    lexema->lexema[lexema->posLexema] = caracter;
                } else {
                    if (lexema->posLexema < 63) {
                        lexema->lexema[lexema->posLexema + 1] = '\0';
                    }
                    if (lexema->posLexema > 0) {
                        if (!strcmp(lexema->lexema, "if")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "else")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "while")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "do")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "for")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "break")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "continue")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "return")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "void")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "int")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "float")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "char")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "double")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "long")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "short")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "enum")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "extern")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "goto")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "register")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "signed")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "unsigned")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "static")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "struct")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "sizeof")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "switch")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "case")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "default")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "const")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "volatile")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "typedef")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else if (!strcmp(lexema->lexema, "include")) {
                            printf("Linha: %d, Lexema: %s, Token: palavra-chave\n", buffer->posFile, lexema->lexema);
                        } else {
                            printf("Linha: %d, Lexema: %s, Token: identificador\n", buffer->posFile, lexema->lexema);
                        }
                    }

                    strcpy(lexema->lexema, " ");
                    lexema->posLexema = -1;
                }
            }
            deallocate_buffer(buffer);
            deallocate_lexema(lexema);
        }
        fclose(source);
    }

    return 0;
}