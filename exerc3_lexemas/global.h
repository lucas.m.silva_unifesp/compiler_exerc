#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// quantidade de caracteres que o buffer suporta
#define bufferSize 64

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

// strutura do buffer
typedef struct bufferBuild
{
    char bufferCarac[bufferSize]; // armazenamento dos caracteres
    int qtdBuffer; // quantidade de caracteres dentro do buffer
    int posBuffer; // posicao do caracter dentro do buffer
    int posFile; // posicao da linha do arquivo
} bufferStruct;

// strutura do lexama e do token
typedef struct lexemaBuild
{
    char lexema[MAXLEXEMALENTH]; // armazenamento do lexema
    int posLexema; // posicao do lexema
    int lexemaToken; // token do lexema
    int line; // linha do arquivo que aparece o lexema
} lexemaStruct;

// ponteiro para o arquivo texto
extern FILE * source;

extern int Error;

#endif // _GLOBAL_H_