#ifndef _FUNCS_H_
#define _FUNCS_H_

/*
    funcao de alocacao do buffer
*/
bufferStruct * allocate_buffer();

lexemaStruct * allocate_lexema ();

/*
    funcao captura uma string do arquivo
    armazenando no buffer, posteriormente
    sera mandado caracter por caracter do buffer
    ate que nao tenha mais nada no buffer para 
    ser mandado
*/
char get_next_char(bufferStruct * buffer);

/*
    funcao de deslocamento do buffer
*/
void deallocate_buffer(bufferStruct * buffer);

void deallocate_lexema(lexemaStruct * lexema);

#endif