#include "global.h"
#include "funcs.h"
#include "scan.h"
#include "util.h"

int is_alpha(char caracter)
{
        return (caracter >= 'a' && caracter <= 'z') || (caracter >= 'A' && caracter <= 'Z');
}

int is_digit(char caracter)
{
        return (caracter >= '0' && caracter <= '9');
}

int is_comands(char c)
{
        return (c >= 0 && c <= 32);
}

int get_token_reserved_word(char * lexema)
{
        if (!strcmp(lexema, "if")) {
            return IF;
        } else if (!strcmp(lexema, "then")) {
            return THEN;
        } else if (!strcmp(lexema, "else")) {
            return ELSE;
        } else if (!strcmp(lexema, "end")) {
            return END;
        } else if (!strcmp(lexema, "repeat")) {
            return REPEAT;
        } else if (!strcmp(lexema, "until")) {
            return UNTIL;
        } else if (!strcmp(lexema, "read")) {
            return READ;
        } else if (!strcmp(lexema, "write")) {
            return WRITE;
        } else {
            return ID;
        }
}

int get_token(bufferStruct * buffer, lexemaStruct * lexema)
{
        char caracter;
        stateType state = START;
        int canPrint = TRUE;
        while (state != DONE) {
                caracter = get_next_char(buffer);
                switch (state) {
                        case START:
                                if (is_alpha(caracter)) {
                                        lexema->posLexema++;
                                        lexema->lexema[lexema->posLexema] = caracter;
                                        state = INID;
                                        lexema->line = buffer->posFile;
                                } else if (is_digit(caracter)) {
                                        lexema->posLexema++;
                                        lexema->lexema[lexema->posLexema] = caracter;
                                        state = INNUM;
                                        lexema->line = buffer->posFile;
                                } else if (caracter == ':') {
                                        lexema->posLexema++;
                                        lexema->lexema[lexema->posLexema] = caracter;
                                        state = INATRIB;
                                        lexema->line = buffer->posFile;
                                } else if (is_comands(caracter)) {
                                        state = DONE;
                                        canPrint = FALSE;
                                } else if (caracter == '{') {
                                        state = INCOMENT;
                                } else {
                                        state = DONE;
                                        switch (caracter) {
                                                case EOF:
                                                        state = DONE;
                                                        lexema->lexemaToken = ENDFILE;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '+':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = PLUS;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '-':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = MINUS;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '*':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = MULT;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '/':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = DIV;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '=':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = EQ;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '<':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = LT;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case '(':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = LPAREN;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case ')':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = RPAREN;
                                                        lexema->line = buffer->posFile;
                                                break;
                                                case ';':
                                                        lexema->posLexema++;
                                                        lexema->lexema[lexema->posLexema] = caracter;
                                                        state = DONE;
                                                        lexema->lexemaToken = SEMI;
                                                        lexema->line = buffer->posFile;
                                                break;

                                                default:
                                                        lexema->lexemaToken = ERRO;
                                                        state = DONE;
                                                        lexema->line = buffer->posFile;
                                                break;
                                        }
                                }
                                break;

                        case INCOMENT:
                                if (caracter == '}') {
                                        state = START;
                                }
                                break;

                        case INNUM:
                                if (is_digit(caracter)) {
                                        lexema->posLexema++;   
                                        lexema->lexema[lexema->posLexema] = caracter;
                                } else {
                                        unget_char(buffer);
                                        state = DONE;
                                        lexema->lexemaToken = NUM;
                                }
                                break;

                        case INID:
                                if (is_alpha(caracter)) {
                                        lexema->posLexema++;
                                        lexema->lexema[lexema->posLexema] = caracter;
                                } else {
                                        unget_char(buffer);
                                        state = DONE;
                                        lexema->lexemaToken = ID;
                                }
                                break;

                        case INATRIB:
                                if (caracter == '=') {
                                        lexema->posLexema++;
                                        lexema->lexema[lexema->posLexema] = caracter;
                                        state = DONE;
                                        lexema->lexemaToken = ASSIGN;
                                } else {
                                        unget_char(buffer);
                                        state = DONE;
                                        lexema->lexemaToken = ERRO;
                                }
                                break;

                        default:
                                break;
                }
        }
        lexema->posLexema++;
        lexema->lexema[lexema->posLexema] = '\0';

        if (lexema->lexemaToken == ID) {
                lexema->lexemaToken = get_token_reserved_word(lexema->lexema);
        }

        if (canPrint) {
                printf("Linha: %d, Lexema: %s, Token: ", lexema->line, lexema->lexema);
                print_token(lexema->lexemaToken);
        }

        strncpy(lexema->lexema, " ", 1);
        lexema->posLexema = -1;

        return lexema->lexemaToken;
}