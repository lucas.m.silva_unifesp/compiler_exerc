#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

// quantidade de caracteres que o buffer suporta
#define bufferSize 64

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

// tokens
#define IF 300
#define THEN 301
#define ELSE 302
#define END 303
#define REPEAT 304
#define UNTIL 305
#define READ 306
#define WRITE 307
#define ID 308
#define NUM 309
#define INT 310
#define BOOL 311
#define PLUS 312
#define MINUS 313
#define MULT 314
#define DIV 315
#define EQ 316
#define LT 317
#define LPAREN 318
#define RPAREN 319
#define ASSIGN 320
#define SEMI 321
#define ENDFILE 322

#define ERRO 400

// estados
typedef enum {
    START, INCOMENT, INNUM, INID, INATRIB, DONE
} stateType;

// strutura do buffer
typedef struct bufferBuild
{
    char bufferCarac[bufferSize]; // armazenamento dos caracteres
    int qtdBuffer; // quantidade de caracteres dentro do buffer
    int posBuffer; // posicao do caracter dentro do buffer
    int posFile; // posicao da linha do arquivo
} bufferStruct;

// strutura do lexama e do token
typedef struct lexemaBuild
{
    char lexema[MAXLEXEMALENTH]; // armazenamento do lexema
    int posLexema; // posicao do lexema
    int lexemaToken; // token do lexema
    int line; // linha do arquivo que aparece o lexema
} lexemaStruct;

// ponteiro para o arquivo texto
extern FILE * source;

// flag de erro
extern int Error;

#endif // _GLOBAL_H_