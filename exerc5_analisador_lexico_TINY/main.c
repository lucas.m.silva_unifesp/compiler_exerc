#include "global.h"
#include "funcs.h"
#include "scan.h"

int Error = FALSE;

FILE * source;

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        Error = TRUE;
    }

    if (!Error) {
        source = fopen(argv[1], "r");

        if (source == NULL) {
            printf("Erro ao alocar memoria para o arquivo texto\n");
            Error = FALSE;
        }
    }

    if (!Error) {
        bufferStruct * buffer = allocate_buffer();
        lexemaStruct * lexema = allocate_lexema();

        if (buffer == NULL || lexema == NULL) {
            printf("Memoria insuficiente para alocacao do buffer");
            exit(1);
        }
        else {
            while (get_token(buffer, lexema) != ENDFILE) {}

            deallocate_buffer(buffer);
            deallocate_lexema(lexema);
        }
        fclose(source);
    }

    return 0;
}