#include "global.h"
#include "funcs.h"

bufferStruct * allocate_buffer ()
{
    bufferStruct * buffer = (bufferStruct *) malloc (sizeof(bufferStruct));

    buffer->qtdBuffer = 0;
    buffer->posBuffer = 0;
    buffer->posFile = 1;

    return buffer;
}

lexemaStruct * allocate_lexema ()
{
    lexemaStruct * lexema = (lexemaStruct *) malloc (sizeof(lexemaStruct));

    lexema->posLexema = -1;

    return lexema;
}

char get_next_char(bufferStruct * buffer)
{
    if (buffer->bufferCarac[0] != EOF) {
        if (buffer->qtdBuffer == buffer->posBuffer || buffer->bufferCarac[buffer->posBuffer] == '\0') {
            if (!feof(source)) {
                strcpy(buffer->bufferCarac, "\0");
                strcpy(buffer->bufferCarac, " ");
                fgets(buffer->bufferCarac, bufferSize, source);
            } else 
                buffer->bufferCarac[0] = EOF;
            
            buffer->qtdBuffer = strlen(buffer->bufferCarac);
            buffer->posBuffer = -1;
        }
        
        buffer->posBuffer++;

        if (buffer->bufferCarac[buffer->posBuffer] == '\n')
            buffer->posFile++;

        return buffer->bufferCarac[buffer->posBuffer];
    } else
        return buffer->bufferCarac[0];
}

void unget_char(bufferStruct * buffer)
{
    if (buffer->bufferCarac[buffer->posBuffer] == '\n')
        buffer->posFile--;

    buffer->posBuffer--;
}

void deallocate_buffer(bufferStruct * buffer)
{
    if (buffer != NULL)
        free(buffer);
}

void deallocate_lexema(lexemaStruct * lexema)
{
    if (lexema != NULL)
        free(lexema);
}