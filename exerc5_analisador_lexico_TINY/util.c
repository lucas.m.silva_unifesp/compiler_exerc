#include "global.h"
#include "util.h"

void print_token(int tokenType) 
{
        switch (tokenType) {
                case IF:
                        printf("IF\n");
                        break;
                case THEN:
                        printf("THEN\n");
                        break;
                case ELSE:
                        printf("ELSE\n");
                        break;
                case END:
                        printf("END\n");
                        break;
                case REPEAT:
                        printf("REPEAT\n");
                        break;
                case UNTIL:
                        printf("UNTIL\n");
                        break;
                case READ:
                        printf("READ\n");
                        break;
                case WRITE:
                        printf("WRITE\n");
                        break;
                case ID:
                        printf("ID\n");
                        break;
                case NUM:
                        printf("NUM\n");
                        break;
                case PLUS:
                        printf("PLUS\n");
                        break;
                case MINUS:
                        printf("MINUS\n");
                        break;
                case MULT:
                        printf("MULT\n");
                        break;
                case DIV:
                        printf("DIV\n");
                        break;
                case EQ:
                        printf("EQ\n");
                        break;
                case LT:
                        printf("LT\n");
                        break;
                case LPAREN:
                        printf("LPAREN\n");
                        break;
                case RPAREN:
                        printf("RPAREN\n");
                        break;
                case ASSIGN:
                        printf("ASSIGN\n");
                        break;
                case SEMI:
                        printf("SEMI\n");
                        break;
                case ENDFILE:
                        printf("ENDFILE\n");
                        break;
                case ERRO:
                        printf("ERRO\n");
                        break;
                default:
                        break;
        }
}