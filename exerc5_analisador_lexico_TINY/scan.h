#ifndef _SCAN_H_
#define _SCAN_H_

int is_alpha(char caracter);

int is_digit(char caracter);

int is_comands(char caracter);

int get_token_reserved_word(char * lexema);

int get_token(bufferStruct * buffer, lexemaStruct * lexema);

#endif